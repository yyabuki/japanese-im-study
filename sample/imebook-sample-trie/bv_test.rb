# -*- encoding: utf-8 -*-

# Copyright (c) 2012 TOKUNAGA Hiroyuki
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the following
# conditions are met:
#
#   1. Redistributions of source code must retain the above Copyright
#   notice, this list of conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above Copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
#   3. Neither the name of the authors nor the names of its contributors
#   may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


require './bv'
require 'minitest/unit'

MiniTest::Unit.autorun

class TestBV < MiniTest::Unit::TestCase

  def setup
    @bv = BV.new
  end

  def test_push
    assert_equal 0, @bv.length

    @bv.push 1
    @bv.push 0

    assert_equal 2, @bv.length
  end

  def test_get
    @bv.push 1
    @bv.push 0
    @bv.push 0
    @bv.push 1
    @bv.push_n 1, 4
    @bv.push 1
    @bv.push 0
    @bv.push 0
    @bv.push 1
    @bv.push_n 1, 4

    assert_equal 1, @bv[0]
    assert_equal 0, @bv[1]
    assert_equal 0, @bv[2]
    assert_equal 1, @bv[3]
    assert_equal 1, @bv[4]
    assert_equal 1, @bv[8]
    assert_equal 0, @bv[9]
    assert_equal 0, @bv[10]
    assert_equal 1, @bv[11]
    assert_equal 1, @bv[15]
  end

  def test_push_error
    assert_raises(ArgumentError) {
      @bv.push 2
    }
  end

  def test_map
    @bv.push 1
    @bv.push 0
    
    r = @bv.map{|x|x}
    assert_equal 1, r[0]
    assert_equal 0, r[1]
  end

end

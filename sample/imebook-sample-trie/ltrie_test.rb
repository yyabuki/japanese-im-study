# -*- encoding: utf-8 -*-

# Copyright (c) 2012 TOKUNAGA Hiroyuki
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the following
# conditions are met:
#
#   1. Redistributions of source code must retain the above Copyright
#   notice, this list of conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above Copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
#   3. Neither the name of the authors nor the names of its contributors
#   may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require './ltrie'
require 'minitest/unit'

MiniTest::Unit.autorun

class TestLTrieBuilder < MiniTest::Unit::TestCase

  def setup
    @builder = LTrieBuilder.new
  end

  def test1a
    words = ["a"]
    @builder.build(words)
    assert_equal [1,0,1,0,0], @builder.lbs.to_a
    assert_equal ["a"], @builder.edge
    assert_equal 1, @builder.terminal.length
    assert_equal [1], @builder.terminal.to_a
    @builder.save("test1.idx")
    @builder.load("test1.idx")
    assert_equal [1,0,1,0,0], @builder.lbs.to_a
    assert_equal ["a"], @builder.edge
    assert_equal 1, @builder.terminal.length
    assert_equal [1], @builder.terminal.to_a

  end

  def test1b
    words = ["ab"]
    @builder.build(words)
    assert_equal [1,0,1,0,1,0,0], @builder.lbs.to_a
    assert_equal ["a","b"], @builder.edge
    assert_equal [0,1], @builder.terminal.to_a
  end

  def test2
    words = ["a", "b"]
    @builder.build(words)
    assert_equal [1,0,1,1,0,0,0], @builder.lbs.to_a
    assert_equal ["a", "b"], @builder.edge
 #   assert_equal [1,1], @builder.terminal.to_a
  end

  def test2b
    words = ["ac", "bc"]
    @builder.build(words)
    assert_equal [1,0,1,1,0,1,0,1,0,0,0], @builder.lbs.to_a
    assert_equal ["a", "b", "c", "c"], @builder.edge
    assert_equal [0,0,1,1], @builder.terminal.to_a

 end

  def test3
    words = ["a", "ab", "b"]
    @builder.build(words)
    assert_equal [1,0,1,1,0,1,0,0,0], @builder.lbs.to_a
    assert_equal ["a", "b", "b"], @builder.edge
    assert_equal [1,1,1], @builder.terminal.to_a
  end

  def test3b
    words = ["ab", "b"]
    @builder.build(words)
    assert_equal [1,0,1,1,0,1,0,0,0], @builder.lbs.to_a
    assert_equal ["a", "b", "b"], @builder.edge
    assert_equal [0,1,1], @builder.terminal.to_a
  end

  def test4o
    words = ["a", "ab", "abc", "b"]
    @builder.build(words)
    assert_equal [1,0,1,1,0,1,0,0,1,0,0], @builder.lbs.to_a
    assert_equal ["a", "b", "b", "c"], @builder.edge
    assert_equal [1,1,1,1], @builder.terminal.to_a
  end

  def test5
    words = ["a", "ba", "baa"]
    @builder.build(words)
    assert_equal [1,0,1,1,0,0,1,0,1,0,0], @builder.lbs.to_a
  end

  def test6
    words = ["aaa", "aac", "ac", "baad", "bab"]
    @builder.build(words)
    assert_equal [1,0,1,1,0,1,1,0,1,0,1,1,0,0,1,1,0,0,0,1,0,0,0], @builder.lbs.to_a
    assert_equal ["a", "b", "a", "c", "a", "a", "c", "a", "b", "d"], @builder.edge
    assert_equal [0,0,0,1,0,1,1,0,1,1], @builder.terminal.to_a
  end

  def test8
    words = ["aaa", "aab", "ac", "b", "cc", "cd"]
    @builder.build(words)
    assert_equal [1,0,1,1,1,0,1,1,0,0,1,1,0,1,1,0,0,0,0,0,0], @builder.lbs.to_a
  end

  def test9
    words = ["あ"]
    @builder.build(words)
    assert_equal [1,0,1,0,1,0,1,0,0], @builder.lbs.to_a
  end

  def test10
    words = ["あい"]
    @builder.build(words)
    assert_equal [1,0,1,0,1,0,1,0,1,0,1,0,1,0,0], @builder.lbs.to_a
  end

end


class TestLTrie < MiniTest::Unit::TestCase

  def setup
    @builder = LTrieBuilder.new
  end

  def test1
    words = ["a"]
    @builder.build(words)
    ltrie = LTrie.new(@builder)
    assert_equal [0], ltrie.common_prefix_search("a")
    assert_equal [], ltrie.common_prefix_search("b")
    assert_equal "a", ltrie.reverse(ltrie.common_prefix_search("a")[0])
  end

  def test2
    words = ["a", "b"]
    @builder.build(words)
    ltrie = LTrie.new(@builder)
    assert_equal [0], ltrie.common_prefix_search("a")
    assert_equal [1], ltrie.common_prefix_search("b")
    assert_equal "a", ltrie.reverse(ltrie.common_prefix_search("a")[0])
    assert_equal "b", ltrie.reverse(ltrie.common_prefix_search("b")[0])
  end

  def test3
    words = ["a", "b", "ab"]
    @builder.build(words)
    ltrie = LTrie.new(@builder)
    assert_equal [0,2], ltrie.common_prefix_search("ab")
    assert_equal [0], ltrie.common_prefix_search("a")
    assert_equal [1], ltrie.common_prefix_search("b")
  end

  def test4x
    words = ["a", "b", "c", "aac", "ab", "add", "abc", "bbb"]
    @builder.build(words)
    ltrie = LTrie.new(@builder)
    assert_equal [0,4,8], ltrie.common_prefix_search("abc")
    assert_equal [1,10], ltrie.common_prefix_search("bbb")
    assert_equal [0,9], ltrie.common_prefix_search("add")

    assert_equal "aac", ltrie.reverse(ltrie.common_prefix_search("aac")[1])
    assert_equal "a", ltrie.reverse(ltrie.common_prefix_search("a")[0])
  end

  def test5x
    words = []
    open("words/words.txt").each{|line|
      line.chomp!
      words.push line if line != ""
    }
    @builder.build(words)

    @builder.save("test5.idx")
    @builder.load("test5.idx")

    ltrie = LTrie.new(@builder)
    words.each{|word|
      p word
      result = ltrie.common_prefix_search(word)
      p result
      assert_operator 1, :<=, result.length # 最低でも1つは結果が返る

      # 結果が共通接頭辞になっているかどうかのチェック
      result.each{|id|
        str = ltrie.reverse(id)
        assert word.start_with? str
      }
    }
  end
end

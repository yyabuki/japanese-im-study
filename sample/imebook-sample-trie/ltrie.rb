# -*- encoding: utf-8 -*-

# Copyright (c) 2012 TOKUNAGA Hiroyuki
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the following
# conditions are met:
#
#   1. Redistributions of source code must retain the above Copyright
#   notice, this list of conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above Copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
#   3. Neither the name of the authors nor the names of its contributors
#   may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require './sbv'

class LTrie

  # sbv == 索引つきのLBS
  # edge == トライのエッジにつく文字の配列
  # terminal == 終端として適切なノードかどうかを保存するビット配列
  def initialize(builder)
    if builder
      @sbv = ISBV.new(builder.lbs)
      # @sbv = SBV.new(builder.lbs)
      # SBVクラスは挙動は同じだが遅い

      @edge = builder.edge
      @terminal = builder.terminal
    end
  end

  def first_child(x)
    y = @sbv.select0(@sbv.rank1(x))+1
    if @sbv[y] == 0
      return :NOT_FOUND
    else
      return y
    end
  end

  def traverse(pos, c)
#    puts "traverse #{pos} #{c}"
    child_pos = first_child(pos)
    if child_pos == :NOT_FOUND
      return :NOT_FOUND
    end

    # m:ノードID
    m = @sbv.rank1(child_pos)-1
#    puts " m #{m} "
    while @sbv[child_pos] == 1

      if @edge[m] == c
        return child_pos
      end

      child_pos += 1
      m += 1
    end
    return :NOT_FOUND
  end

  def common_prefix_search(str)
    # strがfrozen stringであるとき、dupでコピーしておかないと
    # force_encodingできない。
    str = str.dup
    result = []
    n = 0
    str.force_encoding(Encoding::BINARY) # バイト配列として扱いたい

    str.each_byte{|c|
      c = c.chr # each_byteだと0〜255の数字がとれちゃう
      n = traverse(n, c)
#      puts "n: #{n}"
      if n == :NOT_FOUND
        break
      end

      # m: ノードID
      m = @sbv.rank1(n)-1
      if @terminal[m] != 0
        result.push(m)
      end
    }
    return result
  end

  def parent_node(pos)
    parent_pos = @sbv.select1(@sbv.rank0(pos)-1)
  end

  # ノードIDからキー文字列へと戻す。デバッグ用。
  def reverse(node_id)
    result = []
    # 1基準でLBS上の位置に変換する
    pos = @sbv.select1(node_id+1)

    begin
      # 1基準のLBS上の位置からノードIDへ変換する
      m = @sbv.rank1(pos)-1
      result.push @edge[m]
      old_pos = pos
      pos = self.parent_node(pos)
    end while pos != 0

    str = result.reverse.join("")

    # UTF-8文字列であることを想定してエンコーディングを設定している。
    # 必要に応じて適宜書き換えること。
    str.force_encoding(Encoding::UTF_8)
    return str
  end

end


class LTrieBuilder

  attr_reader :lbs, :edge, :terminal

  def initialize()
    @lbs = BV.new
    @edge = []
    @terminal = BV.new
  end

  def load(filename)
    @lbs = BV.new
    @edge = []
    @terminal = BV.new
    open(filename) {|fp|
      size_info = fp.read(4 * 3) # 4バイトを3つ読み込む

      # size_infoは単なるバイト配列（文字列）なので、整数に直す
      # V: 32bit unsigned int (little endian)
      lbs_len, edge_len, terminal_len = size_info.unpack("VVV")

      lbs_byte_len = lbs_len / 8 + 1
      lbs = fp.read(lbs_byte_len)
      @lbs.set_vec(lbs, lbs_len)

      fp.read(edge_len).each_byte{|c|
        @edge.push c.chr
      }

      terminal_byte_len = terminal_len / 8 + 1
      terminal = fp.read(terminal_byte_len)
      @terminal.set_vec(terminal, terminal_len)
    }
  end

  def write_int(fp, i)
    fp.write([i].pack("V")) # V: 32bit unsigned int (little endian)
  end

  def save(filename)
    if not @lbs
      throw Exception.new "this object does not hold any trie data to save."
    end
    open(filename, "w") {|fp|
      write_int(fp, @lbs.length)
      write_int(fp, @edge.length)
      write_int(fp, @terminal.length)
      fp.write(@lbs.get_vec)
      fp.write(@edge.join(""))
      fp.write(@terminal.get_vec)
    }
  end

  # wordsに対して幅優先で木を辿る
  # メモリ節約のために木を作らないで単語の配列
  # をそのままたどってるのでちょっとトリッキー
  def bfs(words, &block)
    queue = [].push [0, 0, words.length]

    while q = queue.shift
      depth = q[0]
      b = q[1]; e = q[2]
      node_starts = [] # 各ノードの開始位置を保存する
      prev = false

      i = b
      while i < e
        if prev != words[i][depth]
          node_starts.push i
          prev = words[i][depth]
        end
        i += 1
      end

      for i in 0..node_starts.length-1
        left = node_starts[i]
        if i + 1 < node_starts.length
          right = node_starts[i+1]
        else
          right = e
        end

        # puts "call #{depth} #{left} #{right}"
        block.call(depth, left, right)

        if words[left][depth+1]
          queue.push [depth+1, left, right]
        elsif words[right-1][depth+1]
          queue.push [depth+1, left+1, right]
        end
      end
    end
  end

  # depth+1段目にある子ノードを返す
  def child_nodes(words, depth, left, right)
    children = []
    for i in left..right-1
      char = words[i][depth+1]

      next if not char
      next if children.length > 0 and children[-1] == char

      children.push char
    end
    return children
  end

  # あるノードが単語終端かどうかを示す配列(@terminal)をビルドする
  def build_terminal(words)
    bfs(words) {|depth, left, right|
#      puts "call d#{depth} l#{left} r#{right}"
      if not words[left][depth+1]
        @terminal.push 1
      else
        @terminal.push 0
      end
    }
  end

  # LBSとedge配列をビルドする
  def build_lbs(words)
    # まずスーパールートのための1と0をpushする
    @lbs.push 1
    @lbs.push 0

    # ルートノードはwords上に表現されていないので特別に処理する
    @lbs.push_n 1, words.map{|word|word[0]}.uniq.length
    @lbs.push 0

    bfs(words) {|depth, left, right|
      nodes = child_nodes(words, depth, left, right)

      @lbs.push_n 1, nodes.length
      @lbs.push 0
      @edge.push words[left][depth]
    }
  end

  def build(words)
    words.sort!
    words.uniq!

    # 空文字列が入ってたら捨てる
    if words[0] == ""
      words.shift
    end

    encoding = words[0].encoding
    words.each{|word|
      word.force_encoding(Encoding::BINARY) # バイト配列として扱いたい
    }

    build_terminal(words)
    build_lbs(words)

    # エンコーディング情報を元に戻す
    words.each{|word|
      word.force_encoding(encoding)
    }
  end

  def should_be_pushed(children, child)
    if children.length == 0
      return true
    elsif children[-1] != child
      return true
    end
    return false
  end

  def one_if_child_not_exist(x)
    if x
      return 0
    else
      return 1
    end
  end

end

# -*- encoding: utf-8 -*-

# Copyright (c) 2012 TOKUNAGA Hiroyuki
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the following
# conditions are met:
#
#   1. Redistributions of source code must retain the above Copyright
#   notice, this list of conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above Copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
#   3. Neither the name of the authors nor the names of its contributors
#   may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require './sbv'
require 'minitest/unit'

MiniTest::Unit.autorun

class TestSBV < MiniTest::Unit::TestCase

  def setup
    bv = BV.new
    bv.push 1
    bv.push 0
    bv.push 1
    bv.push 1
    bv.push 0
    @sbv = SBV.new(bv)
  end

  def test_rank0
    assert_equal 0, @sbv.rank(0, 0)
    assert_equal 0, @sbv.rank(1, 0)
    assert_equal 1, @sbv.rank(2, 0)
    assert_equal 1, @sbv.rank(3, 0)
    assert_equal 1, @sbv.rank(4, 0)
  end

  def test_rank1
    assert_equal 0, @sbv.rank(0, 1)
    assert_equal 1, @sbv.rank(1, 1)
    assert_equal 1, @sbv.rank(2, 1)
    assert_equal 2, @sbv.rank(3, 1)
    assert_equal 3, @sbv.rank(4, 1)
  end

  def test_select0
    assert_equal 1, @sbv.select(0, 0)
    assert_equal 4, @sbv.select(1, 0)
    assert_equal :NOT_FOUND, @sbv.select(2, 0)
  end

  def test_select1
    assert_equal 0, @sbv.select(0, 1)
    assert_equal 2, @sbv.select(1, 1)
    assert_equal 3, @sbv.select(2, 1)
    assert_equal :NOT_FOUND, @sbv.select(3, 1)
  end

end

class TestISBV < MiniTest::Unit::TestCase

  def setup
    bv = BV.new
    @len = 2048
    for i in 0..@len-1
      bv.push (i % 3) % 2
    end

    @isbv = ISBV.new(bv)
    @sbv = SBV.new(bv)
  end

  def test_rank1
    for i in 0..@len-1
      assert_equal @sbv.rank1(i), @isbv.rank1(i)
    end
  end

  def test_rank0
    for i in 0..@len-1
      assert_equal @sbv.rank0(i), @isbv.rank0(i)
    end
  end

  def test_select1
    for i in 0..@len-1
      assert_equal @sbv.select1(i), @isbv.select1(i)
    end
  end

  def test_select0
    for i in 0..@len-1
      assert_equal @sbv.select0(i), @isbv.select0(i)
    end
  end

  def test_small
    bv = BV.new
    bv.push 1
    bv.push 0
    bv.push 1
    bv.push 0

    bv.push 0
    bv.push 1
    bv.push 0
    bv.push 1

    bv.push 1
    bv.push 1
    bv.push 0
    bv.push 0

    bv.push 1
    bv.push 1
    bv.push 0
    bv.push 0

    bv.push 0
    bv.push 1
    bv.push 0
    bv.push 1

    @sbv = SBV.new(bv)
    @isbv = ISBV.new(bv)
    for i in 0..bv.length-1
      assert_equal @sbv.rank1(i), @isbv.rank1(i)
      assert_equal @sbv.rank0(i), @isbv.rank0(i)

      assert_equal @sbv.select1(i), @isbv.select1(i)
      assert_equal @sbv.select0(i), @isbv.select0(i)
    end
  end

  def test_small2
    bv = BV.new

    bv.push 1
    bv.push 0
    bv.push 1
    bv.push 0
    bv.push 0

    @sbv = SBV.new(bv)
    @isbv = ISBV.new(bv)
    for i in 0..bv.length-1
      assert_equal @sbv.rank1(i), @isbv.rank1(i)
      assert_equal @sbv.rank0(i), @isbv.rank0(i)

      assert_equal @sbv.select1(i), @isbv.select1(i)
      assert_equal @sbv.select0(i), @isbv.select0(i)
    end
  end

end

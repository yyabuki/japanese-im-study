# -*- encoding: utf-8 -*-

# Copyright (c) 2012 TOKUNAGA Hiroyuki
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the following
# conditions are met:
#
#   1. Redistributions of source code must retain the above Copyright
#   notice, this list of conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above Copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
#   3. Neither the name of the authors nor the names of its contributors
#   may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# 文字列にビットベクターを埋め込んだもの。BV_debugクラスと比べて
# メモリ使用効率は30倍ぐらいよい。
# 内部では文字列をバイト配列として扱っている。
class BV

  def initialize()
    @vec = ""
    @vec.force_encoding("BINARY") # バイト配列として使うための設定
    @len = 0
  end

  def push(elem)
    unless elem == 0 or elem == 1
      raise ArgumentError.new "this is a bit vector, push 0 or 1."
    end

    if @vec.length * 8 < @len + 1 # 配列サイズが足りなくなったので伸ばす
      @vec += [elem].pack("C")
      @vec += Array.new(128, 0).pack("C*") # 毎回1だけ伸ばしてると遅いので一気に伸ばす
    elsif elem == 1 # 0の場合は@lenを伸ばすだけでいい
      bi = @len / 8
      offset = @len % 8

      # 下の2行は動作は同じだが、下の方がたぶん少し速い
      # @vec[bi] = [@vec.getbyte(bi) | 1 << offset].pack("C")
      @vec.setbyte(bi, @vec.getbyte(bi) | 1 << offset)
    end

    @len += 1
  end

  def push_n(elem, n)
    if elem == 0 or elem == 1
      n.times do
        self.push elem
      end
    else
      raise ArgumentError.new "this is a bit vector, push 0 or 1."
    end
  end

  def getbyte(i)
    return @vec.getbyte(i)
  end

  def [](i)
    if i >= @len
      raise ArgumentError.new "length over."
    end
    block_pos = i / 8
    offset = i % 8
    return (@vec[block_pos].ord & (1 << offset)) >> offset
  end

  def length
    return @len
  end

  def each_with_index(&block)
    for i in 0..@len-1
      block.call(self.[](i), i)
    end
  end

  def each(&block)
    for i in 0..@len-1
      block.call(self.[](i))
    end
  end

  def map(&block)
    r = []
    for i in 0..@len-1
      r.push block.call(self.[](i))
    end
    return r
  end

  def to_a
    return self.map{|x|x}
  end

  def get_vec
    return @vec[0..@len/8]
  end

  def set_vec(vec, len)
    @vec = vec
    @len = len
  end

end

# 最初に作った簡易的なビットベクター、効率は悪いけど実装は簡単。
# getbyteメソッドがないので、実はもうBVクラスと互換性がない。
class BV_debug

  def initialize
    @vec = []
    @len = 0
  end

  def push(elem)
    if elem == 0 or elem == 1
      @vec.push elem
      @len += 1
    else
      raise ArgumentError.new "this is a bit vector, push 0 or 1."
    end
  end

  def push_n(elem, n)
    if elem == 0 or elem == 1
      n.times do
        @vec.push elem
      end
      @len += n
    else
      raise ArgumentError.new "this is a bit vector, push 0 or 1."
    end
  end

  def [](i)
    return @vec[i]
  end

  def length
    return @len
  end

  def each_with_index(&block)
    for i in 0..@len-1
      block.call(@vec[i], i)
    end
  end

  def each(&block)
    for i in 0..@len-1
      block.call(@vec[i])
    end
  end

  def map(&block)
    r = []
    for i in 0..@len-1
      r.push block.call(@vec[i])
    end
    return r
  end

  def to_a
    return self.map{|x|x}
  end

end

# -*- encoding: utf-8 -*-

# Copyright (c) 2012 TOKUNAGA Hiroyuki
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the following
# conditions are met:
#
#   1. Redistributions of source code must retain the above Copyright
#   notice, this list of conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above Copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
#   3. Neither the name of the authors nor the names of its contributors
#   may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require "./bv"

# popcount用のテーブルを作るのに使った関数
# def popcount_raw(num)
#   count = 0
#   for i in 0..7
#     count += (num & 1 << i) >> i
#   end
#   return count
# end

# for i in 0..255
#   print "#{popcount_raw(i)},"
#   puts "" if (i % 16) == 15
# end

$_popcount_table = [0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,
                    1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
                    1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
                    2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                    1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
                    2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                    2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                    3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
                    1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
                    2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                    2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                    3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
                    2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                    3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
                    3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
                    4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8]

def popcount(byte)
  return $_popcount_table[byte]
end

class SBV

  def initialize(bv)
    @bv = bv
  end

  def rank(pos, b)
    # FIXME: throw exception if pos > bv.length
    count = 0
    for i in 0..pos-1
      if @bv[i] == b
        count += 1
      end
    end
    return count
  end

  def select(count, b)
    count += 1
    for i in 0..@bv.length-1
      if @bv[i] == b
        count -= 1
        if count == 0
          return i
        end
      end
    end
    return :NOT_FOUND
  end

  def rank0(pos)
    rank(pos, 0)
  end

  def rank1(pos)
    rank(pos, 1)
  end

  def select0(count)
    select(count, 0)
  end

  def select1(count)
    select(count, 1)
  end

  def [](i)
    @bv[i]
  end

  def to_a
    @bv.to_a
  end

end

# インデックス付きバージョン
class ISBV

  attr_reader :index

  def initialize(bv)
    @bs = 256 # bs: block size
    @bv = bv
    build_index
  end

  # rank1(pos)を各ブロック毎に保存したインデックス
  def build_index()
    @index = [0]
    sum = 0
    for i in 0..(@bv.length-1)/@bs
      bh = i * @bs # bh: block head
      be = [(i+1)*@bs, @bv.length].min # be: block end
      @index.push
      for j in bh..be-1
        sum += @bv[j]
      end
      @index.push sum
    end
  end

  def rank1(pos)
    # FIXME: throw exception if pos > bv.length
    bi = pos / @bs # block index
    bh = bi * @bs  # block head
    count = @index[bi]

    ## ここから高速化のための複雑版
    ## popcountを使って1バイト単位でrankを計算する
    for i in bh/8 .. pos/8-1
      byte = @bv.getbyte(i)
      count += popcount(byte)
    end

    if pos % 8 != 0
      byte = @bv.getbyte(pos / 8)
      shift = 8 - pos % 8
      mask = (0xFF >> shift)
      count += popcount(byte & mask)
    end
    ## 複雑版ここまで

    ## 複雑版をコメントアウトして以下の3行を
    ## 有効にしても挙動は同じ（だけどちょっと遅くなる）
    # for i in bh..pos-1
    #   count += @bv[i]
    # end

    return count
  end

  def rank0(pos)
    return pos - rank1(pos)
  end


  # selectが使う補助関数。インデックスが使えない残りの部分
  # をこれで探す。実際には、可能な部分は先にpopcountで数え
  # た方が速いので、_find2を経由して呼んだほうが良い。
  # select(count, b)は_find(0, count, b)と挙動的には同じ。
  def _find(start_pos, count, b)
    count += 1
    for i in start_pos..@bv.length-1
      if @bv[i] == b
        count -= 1
        if count == 0
          return i
        end
      end
    end
    return :NOT_FOUND
  end

  # _find2はfindの高速化版で、内部で_findを呼んでいる。
  # _find2の代わりに直接_findを呼んでも動くけれど、
  # 遅くなる。
  def _find2(start_pos, count, b)
    count += 1
    for i in start_pos/8 .. @bv.length/8-1
      byte = @bv.getbyte(i)
      if b == 1
        popnum = popcount(byte)
      else
        popnum = 8 - popcount(byte)
      end

      if count - popnum <= 0
        return self._find(i*8, count-1, b)
      end
      count -= popnum
    end
    return self._find((@bv.length/8)*8, count-1, b)
  end

  # selectの実装。rankの情報を保存したインデックスを
  # つかって二分探索でどのブロックかを絞り込んで、
  # _find2を使って残りの部分を探す。
  def select1(count)
    low = 0
    high = @index.length-1
    mid = (low + high)/2
    while (low < high)
      mid = (low + high)/2
      if @index[mid] < count
        low = mid + 1
      else
        high = mid
      end

    end
    sb = [mid-1, 0].max # sb: start block
    _find2(sb * @bs, count - @index[sb], 1)
  end

  # select1と一緒。うまくまとめられなかったので
  # ほぼコピペしちゃった。
  def select0(count)
    low = 0
    high = @index.length-1
    mid = (low + high)/2

    while (low < high)
      mid = (low + high)/2
      if (@bv.length - @index[mid]) < count
        low = mid + 1
      else
        high = mid
      end
    end
    sb = [mid-1, 0].max # sb: start block
    _find2(sb * @bs, count - @index[sb], 0)
  end

  def select(count, b)
    if b == 1
      return select1(count)
    else
      return select0(count)
    end
  end

  def [](i)
    @bv[i]
  end

  def to_a
    @bv.to_a
  end

end

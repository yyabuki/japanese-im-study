#!/usr/bin/ruby -Ku

require "./common"

def read_weight_map(filename, dic)
  w = {}
  w.default = 0.0
  open(filename).each{|line|
    line.chomp!
    a = line.split("\t\t")
    w[a[0]] = a[1].to_f
    b = a[0].split("\t")
    if b.size == 2 and b[0][0] == "S" and b[1][0] == "R"
      word = b[0][1..-1]
      read = b[1][1..-1]
      dic.add(read, word)
    end
  }
  return w
end

feature_funcs = FeatureFuncs.new

node_feature_surface = lambda do |node|
  "S" + node.word
end
feature_funcs.node_features.push node_feature_surface

node_feature_surface = lambda do |node|
  "S" + node.word + "\tR" + node.read
end
feature_funcs.node_features.push node_feature_surface

edge_feature_surface = lambda do |prev_node, node|
  "S" + prev_node.word + "\tS" + node.word
end
feature_funcs.edge_features.push edge_feature_surface


require 'optparse'

opt = OptionParser.new

model_filename = "mk.model"
dic_filename = "juman.dic"

opt.on('-m [filename]', '--model') {|v| model_filename = v}
opt.on('-d [filename]', '--dic') {|v| dic_filename = v}

opt.parse!(ARGV)

dic = Dic.new(dic_filename)
decoder = Decoder.new(feature_funcs)
w = read_weight_map(model_filename, dic)

while line = gets
  line.chomp!
  str = line
  graph = Graph.new(dic, str)
  result = decoder.viterbi(graph, w)
  puts result.map{|x| x[0]}.join(" ")
end

# 日本語入力を支える技術 サンプルソースコード

## 動作環境

Ubuntu Linux 10.10, ruby 1.9.2p0にて動作を確認しました。
なお、Ruby 1.8以前には対応していません。

WindowsやMac OS Xでも動作するはずですが、入出力がUTF-8であるため、標準出力をそのまま表示する場合はターミナルのロケールがUTF-8になっている必要があります。


## 動かし方

### パラメータを学習する

ruby learn.rb train.cps --verbose > train.log

* パラメータはmk.modelというファイルに保存されます（--modelオプションで保存ファイル名を変更できます）。
* マシンのスペックによっては実行に数十分かかります。
* --learner sperceptron というオプションを指定すると、構造化パーセプトロンで学習を行います（デフォルトは構造化SVM）。
* --iteration [数字] というオプションを指定すると、学習を指定の回数だけ繰り返します。繰り返しを行うと変換精度が少し向上しますが、回数の分だけ時間がかかります。
* 実行されている様子を眺めたい場合は、末尾の「 > train.log」を削除して実行してください。


### 文章を変換してみる

ruby test.rb

* 標準入力からかな文字列を受け取り、変換結果を標準出力へと出力します。
* 上記コマンドを実行するとプロンプトが次行の先頭で点滅している状態になりますので、この状態で適当にひらがなで文字を入力します。（ひらがなとして確定し、さらにEnterキーを押してください）。
* 終了するときはCtrl-Cを押してください。
* パラメータはmk.modelというファイルからロードされます（--modelオプションでロードするファイル名を変更できます）。


### 変換精度を求める

ruby eval.rb test.cps --verbose > eval.log

* 適合率（precision）と再現率（recall）が標準出力の末尾に出力されます（上記の場合だと、eval.logの末尾に出力されます）。
* このプログラムもマシンのスペックによっては実行に数十分かかります。
* パラメータはmk.modelというファイルからロードされます（--modelオプションでロードするファイル名を変更できます）。
* こちらも、実行中の様子を眺めたい場合は、末尾の「 > eval.log」を削除して実行してください。


## juman.dicの著作権

juman.dicは形態素解析器JUMAN（http://nlp.ist.i.kyoto-u.ac.jp/index.php?JUMAN）の辞書から、表記と読み仮名だけを取り出したものです。JUMANの辞書はNew BSDLなので、juman.dicもNew BSDLとします。


## train.cpsとtest.cpsの著作権

train.cpsとtest.cpsはWikipedia日本語版（http://ja.wikipedia.org）からサンプリングされた記事データにKyTeaで解析を行い、その結果をある程度人手で修正したものです。Wikipedia日本語版はクリエイティブ・コモンズ・ライセンス（CC-BY-SA）であるため、train.cpsとtest.cpsもそちらに従うものとします。


## その他、使用に関するご注意
* 本サンプルプログラムに収録されているデータ、プログラムの著作権は、すべてその作者あるいは開発元が所有します。なお、juman.dicおよびtrain.cpsとtest.cpsの著作権に関しては、それぞれの項目に従ってください。

* 本サンプルのうち、プログラム本体については以下のライセンス(New BSDL)で配布するものとします。
――――――――――
  Copyright (c) 2012, TOKUNAGA Hiroyuki

 Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above Copyright
notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
Copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the
distribution.

   3. Neither the name of the authors nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
――――――――――

* 本サンプルプログラムを使用した結果生じたいかなる直接的・間接的損害に関して、プログラムの作者、開発元、提供元、および株式会社技術評論社は一切の責任を負いません。また、使用方法およびサポートに関しても一切の責任を負いません。使用はご自身の責任で行ってください。

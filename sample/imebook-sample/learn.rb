#!/usr/bin/ruby -Ku

require "./common"

class SSVMDecoder < Decoder

  def initialize(feature_funcs)
    super(feature_funcs)
    @penalty = 0.05
  end

  def is_correct_node(node, gold)
    correct_node = gold[node.endpos]
    if correct_node and node.word == correct_node.word
      true
    else
      false
    end
  end

  def is_correct_edge(prev_node, node, gold)
    is_correct_node(node, gold) and is_correct_node(prev_node, gold)
  end

  def get_node_score(node, gold, w)
    score = 0.0
    if is_correct_node(node, gold)
      score -= @penalty
    end

    score += super(node, gold, w)
    return score
  end

  def get_edge_score(prev_node, node, gold, w)
    score = 0.0
    if is_correct_edge(prev_node, node, gold)
      score -= @penalty
    end

    score += super(prev_node, node, gold, w)
    return score
  end

end

class Learner
  def initialize(dic, feature_funcs, verbose_mode)
    @w = {}
    @w.default = 0.0
    @dic = dic
    @decoder = Decoder.new(feature_funcs)
    @feature_funcs = feature_funcs
    @verbose_mode = verbose_mode
    @learning_rate = 0.1
  end

  def learn(sentence)
    str = sentence.map{|x|x[1]}.join("")
    graph = Graph.new(@dic, str)
    result = @decoder.viterbi(graph, @w)
    if graph != result
      update_parameters!(sentence, result)
    end
    if @verbose_mode
#      puts sentence.map{|x|x[0]}.join(" ")
      puts result.map{|x|x[0]}.join(" ")
    end
  end

  def convert_to_nodes(sentence)
    ret = []
    bos = Node.new("", "", 0)
    ret.push bos
    i = 0
    prev = bos

    sentence.each{|x|
      i += x[1].length
      node = Node.new(x[0], x[1], i)
      node.prev = prev
      ret.push node
      prev = node
    }

    eos = Node.new("", "", i+1)
    eos.prev = prev
    ret.push eos

    return ret
  end

  def convert(str)
    graph = Graph.new(@dic, str)
    result = @decoder.viterbi(graph, @w)
  end

  def update_node_score!(node, diff)
    @dic.add(node.read, node.word)
    @feature_funcs.node_features.each{|func|
      feature = func.call(node)
      if @w.has_key? feature
        @w[feature] += diff
      else
        @w[feature] = diff
      end
    }
  end

  def update_edge_score!(prev_node, node, diff)
    return if not prev_node
    @feature_funcs.edge_features.each{|func|
      feature = func.call(prev_node, node)
      if @w.has_key? feature
        @w[feature] += diff
      else
        @w[feature] = diff
      end
    }
  end

  def update_parameters_body!(sentence, diff)
    nodes = convert_to_nodes(sentence)
    prev_node = false
    nodes.each{|node|
      update_node_score!(node, diff)
      update_edge_score!(prev_node, node, diff)
      prev_node = node
    }
  end

  def update_parameters!(sentence, result)
    update_parameters_body!(sentence, @learning_rate)
    update_parameters_body!(result, -1 * @learning_rate)
  end

  def save(filename)
    open(filename, "w") {|fp|
      @w.each{|feature, value|
        fp.puts feature + "\t\t" + value.to_s
      }
    }
  end
end

class SPerceptron < Learner
end

class SSVM < Learner
  def initialize(dic, feature_funcs, verbose_mode)
    super(dic, feature_funcs, verbose_mode)
    @decoder = SSVMDecoder.new(feature_funcs)
    @decoder_orig = Decoder.new(feature_funcs)

    @last_updated = {}
    @last_updated.default = 0
    @updated_count = 0
    @lambda = 1.0e-22
  end

  def convert_to_gold_standard(sentence)
    ret = []
    nodes = convert_to_nodes(sentence)
    nodes.each{|node|
      ret[node.endpos] = node
    }
    return ret
  end

  def sign(x)
    if x >= 0 then 1 else -1 end
  end

  def clip(a, b)
    sign(a) * [a.abs - b, 0].max
  end

  def regularize_feature!(feature)
    if @w.has_key? feature
      last_updated = @last_updated[feature]
      new_val = clip(@w[feature], @lambda * (@updated_count - last_updated))
      if new_val.abs < 1.0e-10
        @w.delete(feature)
      else
        @w[feature] = new_val
      end
      @last_updated[feature] = @updated_count
    end
  end

  def regularize_node!(node)
    @feature_funcs.node_features.each{|func|
      feature = func.call(node)
      regularize_feature!(feature)
    }
  end

  def regularize_edge!(prev_node, node)
    @feature_funcs.edge_features.each{|func|
      feature = func.call(prev_node, node)
      regularize_feature!(feature)
    }
  end

  # graphに出てくるところだけ正則化をかける
  # 雑誌のナイーブなコードでも動作速度以外は問題ないが、今回は遅すぎるのでこのテクニックを使う。
  # 詳しくは Efficient Online and Batch Learning Using Forward Backward Splitting; John Duchi, Yoram Singer; Journal of Machine Learning Research 10(Dec):2899-2934, 2009 を参照。
  def regularize!(graph)
    return
    graph.nodes.each{|nodes|
      nodes.each{|node|
        next if node.is_bos?
        regularize_node!(node)
        graph.get_prevs(node).each{|prev_node|
          regularize_edge!(prev_node, node)
        }
      }
    }
  end

  # ファイルにパラメーターを保存する前に正則化をかけるために使う
  def regularize_all!()
    @w.each{|feature, value|
      regularize_feature!(feature)
    }
  end

  def learn(sentence)
    str = sentence.map{|x|x[1]}.join("")
    graph = Graph.new(@dic, str)
    regularize!(graph)

    gold_standard = convert_to_gold_standard(sentence)
    result = @decoder.viterbi(graph, @w, gold_standard)
    if graph != result
      update_parameters!(sentence, result)
    end
    if @verbose_mode
      puts sentence.map{|x|x[0]}.join(" ")
      puts result.map{|x|x[0]}.join(" ")
    end
    @updated_count += 1
  end

  def convert(str)
    graph = Graph.new(@dic, str)
    result = @decoder_orig.viterbi(graph, @w)
  end

  def save(filename)
    regularize_all!
    super(filename)
  end

end

feature_funcs = FeatureFuncs.new

node_feature_surface = lambda do |node|
  "S" + node.word
end
feature_funcs.node_features.push node_feature_surface

node_feature_surface = lambda do |node|
  "S" + node.word + "\tR" + node.read
end
feature_funcs.node_features.push node_feature_surface


edge_feature_surface = lambda do |prev_node, node|
  "S" + prev_node.word + "\tS" + node.word
end
feature_funcs.edge_features.push edge_feature_surface

require 'optparse'

opt = OptionParser.new

model_filename = "mk.model"
dic_filename = "juman.dic"
learner_type = "ssvm"
verbose_mode = false
iteration_num = 1

opt.banner = "learn.rb [options] corpus_filename"
opt.on('-m [filename]', '--model') {|v| model_filename = v}
opt.on('-d [filename]', '--dic') {|v| dic_filename = v}
opt.on('-l [ssvm or sperceptron]', '--learner') {|v| learner_type = v}
opt.on('-v', '--verbose') {|v| verbose_mode = true}
opt.on('-i [integer]', '--iteration') {|v| iteration_num = v.to_i}

opt.parse!(ARGV)

dic = Dic.new(dic_filename)

learner = false
if learner_type == "ssvm"
  learner = SSVM.new(dic, feature_funcs, verbose_mode)
elsif learner_type == "sperceptron"
  learner = SPerceptron.new(dic, feature_funcs, verbose_mode)
else
  STDERR.puts "learner must be 'ssvm' or 'sperceptron'."
  STDERR.puts opt.help
  exit -1
end

corpus_filename = ARGV.shift
if !corpus_filename
  STDERR.puts "corpus filename not found"
  exit -1
end

for i in 1..iteration_num
  open(corpus_filename).each{|line|
    line.chomp!
    puts line if verbose_mode
    sentence = line.split(" ").map{|x|x.split("/")}
    learner.learn(sentence)
  }
end

learner.save(model_filename)

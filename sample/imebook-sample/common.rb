#!/usr/bin/ruby -Ku

class Dic
  def initialize(filename)
    @ht = {}
    @ht.default = []
    open(filename).each{|line|
      line.chomp!
      a = line.split("\t")
#      puts a[0] + "\t" + a[1]
      add(a[0], a[1])
    }
  end

  def add(read, word)
      if @ht.has_key? read
        if not @ht[read].index word
          @ht[read].push word
        end
      else
        @ht[read] = [word]
      end
  end

  def common_prefix_search(str, max)
    result = []
    for i in 1..[str.length, max].min
      read = str[0, i]
      @ht[read].each{|word|
        result.push [read, word]
      }
    end
    result
  end

  def find(str)
    if @ht.has_key? str
      @ht[str]
    else
      []
    end
  end

end

class Node
  attr_reader :endpos, :word, :read, :score, :prev
  attr_writer :score, :prev

  def initialize(word, read, endpos)
    @word = word
    @read = read
    @endpos = endpos
    @score = 0.0
    @prev = false
  end

  def length
    read.length
  end

  def is_bos?
    if endpos == 0 then true else false end
  end

  def is_eos?
    if read.length == 0 and endpos != 0 then true else false end
  end
end

class Graph
  attr_reader :nodes, :eos
  def initialize(dic, str)
    @nodes = []
    # push BOS
    bos = Node.new("", "", 0)
    @nodes[0] = [bos]

    # push BOS
    @eos = Node.new("", "", str.length+1)
    @nodes[str.length+1] = [eos]

    for i in 0..str.length-1
      for j in i+1..[str.length, i+16].min
        read = str[i...j]
        dic.find(read).each{|word|
          node = Node.new(word, read, j)
          if @nodes[j]
            @nodes[j].push node
          else
            @nodes[j] = [node]
          end
        }
      end
      read = str[i...i+1]
      if read != ""
        #        puts "put "+ read
        node = Node.new(read, read, i+1)
        if @nodes[i+1]
          @nodes[i+1].push node
        else
          @nodes[i+1] = [node]
        end
      end
    end
  end

  def get_prevs(node)
    if node.is_eos?
      # eosはlengthが0なので特殊な処理が必要
      startpos = node.endpos - 1
      nodes[startpos]
    elsif node.is_bos?
      # bosはそれより前のノードがないので特殊な処理が必要
      []
    else
      startpos = node.endpos - node.length
      nodes[startpos]
    end
  end
end

class FeatureFuncs
  attr_reader :node_features, :edge_features
  attr_writer :node_features, :edge_features
  def initialize
    @node_features = []
    @edge_features = []
  end
end

class Decoder
  def initialize(feature_funcs)
    @feature_funcs = feature_funcs
  end

  def get_node_score(node, gold, w)
    score = 0.0
    @feature_funcs.node_features.each{|func|
      feature = func.call(node)
      score += w[feature]
    }
    return score
  end

  def get_edge_score(prev_node, node, gold, w)
    score = 0.0
    @feature_funcs.edge_features.each{|func|
      feature = func.call(prev_node, node)
      score += w[feature]
    }
    return score
  end

  def viterbi(graph, w, gold=false)

    graph.nodes.each{|nodes|
      nodes.each{|node|
        next if node.is_bos?
        node.score = - 1000000.0
        node_score_cache = get_node_score(node, gold, w)

        graph.get_prevs(node).each{|prev_node|
          tmp_score = prev_node.score + get_edge_score(prev_node, node, gold, w) + node_score_cache
          if tmp_score >= node.score
            node.score = tmp_score
            node.prev = prev_node
          end
        }
      }
    }

    result = []
    node = graph.eos.prev
    while not node.is_bos?
      #    puts node.to_s + node.word + "\t" + node.endpos.to_s
      #    puts result
      result.push [node.word, node.read]
      node = node.prev
    end

    return result.reverse
  end
end


#!/usr/bin/ruby -Ku

require "./common"

def max3(a, b, c)
  if a > b and a > c
    return a
  elsif b > c
    return b
  else
    return c
  end
end

require "pp"

def calc_lcs(str1, str2)
  m = str1.size
  n = str2.size
  table = Array.new
  table = Array.new(n+1).map!{ Array.new(m+1, 0) }

  for j in 1 .. n
    for i in 1 .. m
      same = 0
      same = 1 if str1[i-1] == str2[j-1]
      table[j][i] = max3(table[j-1][i-1] + same, 
                         table[j-1][i],
                         table[j][i-1])
    end
  end

  return table[n][m]
end

def read_weight_map(filename, dic)
  w = {}
  w.default = 0.0
  open(filename).each{|line|
    line.chomp!
    a = line.split("\t\t")
    w[a[0]] = a[1].to_f
    b = a[0].split("\t")
    if b.size == 2 and b[0][0] == "S" and b[1][0] == "R"
      word = b[0][1..-1]
      read = b[1][1..-1]
      dic.add(read, word)
    end
  }
  return w
end

feature_funcs = FeatureFuncs.new

node_feature_surface = lambda do |node|
  "S" + node.word
end
feature_funcs.node_features.push node_feature_surface

node_feature_surface = lambda do |node|
  "S" + node.word + "\tR" + node.read
end
feature_funcs.node_features.push node_feature_surface

edge_feature_surface = lambda do |prev_node, node|
  "S" + prev_node.word + "\tS" + node.word
end
feature_funcs.edge_features.push edge_feature_surface


require 'optparse'

opt = OptionParser.new

model_filename = "mk.model"
dic_filename = "juman.dic"
verbose_mode = false

opt.on('-m [filename]', '--model') {|v| model_filename = v}
opt.on('-d [filename]', '--dic') {|v| dic_filename = v}
opt.on('-v', '--verbose') {|v| verbose_mode = true}

opt.parse!(ARGV)

corpus_filename = ARGV.shift
if !corpus_filename
  STDERR.puts "corpus filename not found"
  STDERR.puts opt.help
  exit -1
end

dic = Dic.new(dic_filename)
decoder = Decoder.new(feature_funcs)
w = read_weight_map(model_filename, dic)

lcs_sum = 0
sys_sum = 0
cps_sum = 0

open(corpus_filename).each{|line|
  line.chomp!
  puts line if verbose_mode
  sentence = line.split(" ").map{|x|x.split("/")}
  str = sentence.map{|x|x[1]}.join("")
  graph = Graph.new(dic, str)
  result = decoder.viterbi(graph, w)

  gold = sentence.map{|x| x[0]}.join("")
  ret = result.map{|x| x[0]}.join("")
  lcs = calc_lcs(gold, ret)
  lcs_sum += lcs
  cps_sum += gold.size
  sys_sum += ret.size
  if verbose_mode
    puts result.map{|x|x[0]}.join(" ")
    puts ""
  end
}

puts sprintf("lcs/sys(precision):%2.2f", (lcs_sum.to_f / sys_sum * 100))
puts sprintf("lcs/cps(recall):%2.2f", (lcs_sum.to_f / cps_sum * 100))
